﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Inmergers.Data.Infrastructure.repository
{
    public interface IDatabaseFactory
    {
        DbContext GetDbContext();
    }
}
