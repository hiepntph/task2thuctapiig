﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Inmergers.Data.Data.Entity
{
    public class NguoiDung
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string TenTaiKhoan { get; set; }
        [Required]
        public string MatKhau { get; set; }
        public string HoTen { get; set; }
        public string Email { get; set; }
        public DateTime NgayTao { get; set; }

    }
}
