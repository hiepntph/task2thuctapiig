﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Inmergers.Data.Data.Entity
{
    public class ChiTietPhieuMuaHang
    {
        [Key]
        public Guid Id { get; set; }
        public int SoLuong { get; set; }

        [ForeignKey("IdPhieuDangKiMuaHang")]
        public virtual PhieuDangKiMuaHang PhieuDangKiMuaHang{ get; set; }
        public Guid IdPhieuDangKiMuaHang { get; set; }
       

        [ForeignKey("IdMatHang")]
        public virtual MatHang MatHang { get; set; }
        public Guid IdMatHang { get; set; }
        
    }
}
