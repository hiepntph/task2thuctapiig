﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Inmergers.Data.Data.Entity
{
    public class MatHang
    {
        [Key]
         public Guid Id { get; set; }
         public string tenMatHang { get; set; }
         public int Stt { get; set; }
         public int soLuong { get; set; }
         public ICollection<ChiTietPhieuMuaHang> ChiTietPhieuMuaHang { get; set; }

    }
}
