﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Inmergers.Data.Data.Entity
{
    public class PhieuDangKiMuaHang
    {
        [Key]
        public Guid Id { get; set; }
        [ForeignKey("IdNhanVien")]
        public virtual NhanVien NhanVien { get; set; }
        public Guid IdNhanVien { get; set; }
        public DateTime NgayTao { get; set; }
        public ICollection<ChiTietPhieuMuaHang> ChiTietPhieuMuaHang { get; set; } = new List<ChiTietPhieuMuaHang>();

    }

}
