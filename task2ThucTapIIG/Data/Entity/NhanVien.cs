﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Inmergers.Data.Data.Entity
{
    public class NhanVien 
    {
        public Guid Id { get; set; }
        public string Hoten { get; set; }
        public string TenTk { get; set ; }
        public string matkhau { get; set ; }
        public string gmail { get; set ; }
        public DateTime ngaytao { get; set ; }
        public Guid IdBoPhan { get; set; }
        [ForeignKey("IdBoPhan")]
        public boPhan boPhan { get; set; }
        
        
    }
}
