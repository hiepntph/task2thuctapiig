﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Inmergers.Data.Migrations
{
    public partial class lan1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "boPhans",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Stt = table.Column<int>(nullable: false),
                    tenNguoiDungDau = table.Column<string>(nullable: true),
                    tenBoPhan = table.Column<string>(nullable: true),
                    ngayTao = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_boPhans", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "matHangs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    tenMatHang = table.Column<string>(nullable: true),
                    Stt = table.Column<int>(nullable: false),
                    soLuong = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_matHangs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "nguoiDungs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TenTaiKhoan = table.Column<string>(nullable: false),
                    MatKhau = table.Column<string>(nullable: false),
                    HoTen = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    NgayTao = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_nguoiDungs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NhanViens",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Hoten = table.Column<string>(nullable: true),
                    TenTk = table.Column<string>(nullable: true),
                    matkhau = table.Column<string>(nullable: true),
                    gmail = table.Column<string>(nullable: true),
                    ngaytao = table.Column<DateTime>(nullable: false),
                    IdBoPhan = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NhanViens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NhanViens_boPhans_IdBoPhan",
                        column: x => x.IdBoPhan,
                        principalTable: "boPhans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "phieuDangKiMuaHangs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IdNhanVien = table.Column<Guid>(nullable: false),
                    NgayTao = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_phieuDangKiMuaHangs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_phieuDangKiMuaHangs_NhanViens_IdNhanVien",
                        column: x => x.IdNhanVien,
                        principalTable: "NhanViens",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ChiTietPhieuMuaHangs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    SoLuong = table.Column<int>(nullable: false),
                    IdPhieuDangKiMuaHang = table.Column<Guid>(nullable: false),
                    IdMatHang = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChiTietPhieuMuaHangs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChiTietPhieuMuaHangs_matHangs_IdMatHang",
                        column: x => x.IdMatHang,
                        principalTable: "matHangs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ChiTietPhieuMuaHangs_phieuDangKiMuaHangs_IdPhieuDangKiMuaHang",
                        column: x => x.IdPhieuDangKiMuaHang,
                        principalTable: "phieuDangKiMuaHangs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "boPhans",
                columns: new[] { "Id", "Stt", "ngayTao", "tenBoPhan", "tenNguoiDungDau" },
                values: new object[,]
                {
                    { new Guid("c020f595-16f8-40a8-bd45-1501b517735f"), 1, new DateTime(2023, 2, 9, 8, 37, 51, 499, DateTimeKind.Local).AddTicks(7216), "Kinh doanh", "Hiep" },
                    { new Guid("638ee9c7-d468-4be6-999c-0b313144a58c"), 2, new DateTime(2023, 2, 9, 8, 37, 51, 499, DateTimeKind.Local).AddTicks(7956), "IT", "Hai" },
                    { new Guid("91feaa95-9a22-406d-ac57-c33ac53670ca"), 3, new DateTime(2023, 2, 9, 8, 37, 51, 499, DateTimeKind.Local).AddTicks(7979), "Chien Luoc", "Bach" }
                });

            migrationBuilder.InsertData(
                table: "matHangs",
                columns: new[] { "Id", "Stt", "soLuong", "tenMatHang" },
                values: new object[,]
                {
                    { new Guid("b86ad435-a670-4a12-aaaf-9e4b9d033463"), 1, 0, "Kiem" },
                    { new Guid("fbd25f0c-6812-4eb8-9597-952be09ed8a8"), 2, 0, "Dao" },
                    { new Guid("e6740993-4baa-4f48-b2d8-912fc1bb16f1"), 3, 0, "Pho" }
                });

            migrationBuilder.InsertData(
                table: "nguoiDungs",
                columns: new[] { "Id", "Email", "HoTen", "MatKhau", "NgayTao", "TenTaiKhoan" },
                values: new object[] { new Guid("c020f595-16f8-40a8-bd45-1501b517735f"), "taihiep312@gmail.com", "Nguyễn Tài Hiệp", "admin", new DateTime(2023, 2, 9, 8, 37, 51, 497, DateTimeKind.Local).AddTicks(7650), "admin" });

            migrationBuilder.InsertData(
                table: "NhanViens",
                columns: new[] { "Id", "Hoten", "IdBoPhan", "TenTk", "gmail", "matkhau", "ngaytao" },
                values: new object[] { new Guid("31445c91-cfcc-488f-944b-7b13ae051b3e"), "bach", new Guid("c020f595-16f8-40a8-bd45-1501b517735f"), "bach", "bach312@gmail.com", "bach", new DateTime(2023, 2, 9, 8, 37, 51, 500, DateTimeKind.Local).AddTicks(1326) });

            migrationBuilder.InsertData(
                table: "NhanViens",
                columns: new[] { "Id", "Hoten", "IdBoPhan", "TenTk", "gmail", "matkhau", "ngaytao" },
                values: new object[] { new Guid("62441159-c1cf-47e9-b06c-c8858d7012aa"), "hiep", new Guid("638ee9c7-d468-4be6-999c-0b313144a58c"), "hiep", "hiep312@gmail.com", "hiep", new DateTime(2023, 2, 9, 8, 37, 51, 500, DateTimeKind.Local).AddTicks(756) });

            migrationBuilder.InsertData(
                table: "NhanViens",
                columns: new[] { "Id", "Hoten", "IdBoPhan", "TenTk", "gmail", "matkhau", "ngaytao" },
                values: new object[] { new Guid("5281c629-97f7-4ea8-a08b-2b60882727a3"), "hai", new Guid("638ee9c7-d468-4be6-999c-0b313144a58c"), "hai", "hai312@gmail.com", "hai", new DateTime(2023, 2, 9, 8, 37, 51, 500, DateTimeKind.Local).AddTicks(1305) });

            migrationBuilder.InsertData(
                table: "phieuDangKiMuaHangs",
                columns: new[] { "Id", "IdNhanVien", "NgayTao" },
                values: new object[] { new Guid("91feaa95-9a22-406d-ac57-c33ac53670ca"), new Guid("31445c91-cfcc-488f-944b-7b13ae051b3e"), new DateTime(2023, 2, 9, 8, 37, 51, 500, DateTimeKind.Local).AddTicks(2558) });

            migrationBuilder.InsertData(
                table: "phieuDangKiMuaHangs",
                columns: new[] { "Id", "IdNhanVien", "NgayTao" },
                values: new object[] { new Guid("c020f595-16f8-40a8-bd45-1501b517735f"), new Guid("62441159-c1cf-47e9-b06c-c8858d7012aa"), new DateTime(2023, 2, 9, 8, 37, 51, 500, DateTimeKind.Local).AddTicks(2267) });

            migrationBuilder.InsertData(
                table: "phieuDangKiMuaHangs",
                columns: new[] { "Id", "IdNhanVien", "NgayTao" },
                values: new object[] { new Guid("638ee9c7-d468-4be6-999c-0b313144a58c"), new Guid("5281c629-97f7-4ea8-a08b-2b60882727a3"), new DateTime(2023, 2, 9, 8, 37, 51, 500, DateTimeKind.Local).AddTicks(2534) });

            migrationBuilder.InsertData(
                table: "ChiTietPhieuMuaHangs",
                columns: new[] { "Id", "IdMatHang", "IdPhieuDangKiMuaHang", "SoLuong" },
                values: new object[] { new Guid("c020f595-16f8-40a8-bd45-1501b517735f"), new Guid("b86ad435-a670-4a12-aaaf-9e4b9d033463"), new Guid("91feaa95-9a22-406d-ac57-c33ac53670ca"), 30 });

            migrationBuilder.InsertData(
                table: "ChiTietPhieuMuaHangs",
                columns: new[] { "Id", "IdMatHang", "IdPhieuDangKiMuaHang", "SoLuong" },
                values: new object[] { new Guid("638ee9c7-d468-4be6-999c-0b313144a58c"), new Guid("e6740993-4baa-4f48-b2d8-912fc1bb16f1"), new Guid("638ee9c7-d468-4be6-999c-0b313144a58c"), 20 });

            migrationBuilder.InsertData(
                table: "ChiTietPhieuMuaHangs",
                columns: new[] { "Id", "IdMatHang", "IdPhieuDangKiMuaHang", "SoLuong" },
                values: new object[] { new Guid("91feaa95-9a22-406d-ac57-c33ac53670ca"), new Guid("e6740993-4baa-4f48-b2d8-912fc1bb16f1"), new Guid("638ee9c7-d468-4be6-999c-0b313144a58c"), 10 });

            migrationBuilder.CreateIndex(
                name: "IX_ChiTietPhieuMuaHangs_IdMatHang",
                table: "ChiTietPhieuMuaHangs",
                column: "IdMatHang");

            migrationBuilder.CreateIndex(
                name: "IX_ChiTietPhieuMuaHangs_IdPhieuDangKiMuaHang",
                table: "ChiTietPhieuMuaHangs",
                column: "IdPhieuDangKiMuaHang");

            migrationBuilder.CreateIndex(
                name: "IX_NhanViens_IdBoPhan",
                table: "NhanViens",
                column: "IdBoPhan");

            migrationBuilder.CreateIndex(
                name: "IX_phieuDangKiMuaHangs_IdNhanVien",
                table: "phieuDangKiMuaHangs",
                column: "IdNhanVien");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ChiTietPhieuMuaHangs");

            migrationBuilder.DropTable(
                name: "nguoiDungs");

            migrationBuilder.DropTable(
                name: "matHangs");

            migrationBuilder.DropTable(
                name: "phieuDangKiMuaHangs");

            migrationBuilder.DropTable(
                name: "NhanViens");

            migrationBuilder.DropTable(
                name: "boPhans");
        }
    }
}
