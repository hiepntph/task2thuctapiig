﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bogus;
using Inmergers.Data.Data.DbContext;
using Inmergers.Data.Data.Entity;

namespace Inmergers.APi.VatTu.FakeData
{
    public class Seedding
    {
        private readonly vattudbContext _Context;

        public Seedding( vattudbContext dbContext)
        {
            _Context = dbContext;
        }
        private List<Guid> ListGuid(int n)
        {
            var Guids = new List<Guid>();
            while (Guids.Count < n)
            {
                var newGuid = Guid.NewGuid();
                if (!Guids.Contains(newGuid))
                    Guids.Add(newGuid);
            }
            return Guids;
        }
        public Faker faker = new Faker();
        public async Task SeeddingBoPhan()
        {
            // var listBoPhanGuid = new List<Guid>();
            // listBoPhanGuid.Add(Guid.NewGuid());
            
            int soluong = 15;
            var ListId = ListGuid(soluong);
            for (int i = 0; i < soluong; i++)
            {
               
                _Context.boPhans.Add(new boPhan()
                        {
                    Id = ListId[i],
                    tenBoPhan = faker.Name.FirstName(),
                    Stt = faker.Random.Number(40),
                    ngayTao = faker.Date.Between(DateTime.Today.AddMonths(-10), DateTime.Now ),
                    tenNguoiDungDau = faker.Name.FullName()
                        }
                        );
                _Context.SaveChangesAsync();
            }
        }

        public async Task SeeddingNhanVien()
        {
            int soluong = 1000;
            var ListId = ListGuid(soluong);
            var BoPhans = _Context.boPhans.ToList();
            var DsIdBoPhan = BoPhans.Select(x => x.Id).ToList();
            for (int i = 0; i < soluong; i++)
            {
                _Context.NhanViens.Add(new NhanVien()
                {
                    Id = ListId[i],
                    ngaytao = faker.Date.Between(DateTime.Today.AddMonths(-1), DateTime.Now),
                    TenTk = faker.Name.FirstName(),
                    matkhau = faker.Internet.Password(),
                    Hoten = faker.Name.FullName(),
                    gmail = faker.Internet.Email(),
                    IdBoPhan = faker.PickRandom(DsIdBoPhan)
                });
                _Context.SaveChangesAsync();
            }

        }

        public async Task SeeddingSanPham()
        {
            int soluong = 1000;
            int stt = 1;
            var ListId = ListGuid(soluong);
            for (int i = 0; i < soluong; i++)
            {
                _Context.matHangs.Add(new MatHang()
                {
                    Id = ListId[i],
                    Stt = stt++,
                    soLuong = faker.Random.Number(0, 100),
                    tenMatHang = faker.Name.FirstName(),
                });
                _Context.SaveChangesAsync();
            }

        }

        public async Task SeeddingPhieuDangKiMuahang()
        {
            int soluong = 1000;
            var ListIdPhieu = ListGuid(soluong);
            var ListIdCtPhieu = ListGuid(soluong * 10);
        
            var Mathangs = _Context.matHangs;
            var DsIdMathang = Mathangs.Select(x => x.Id).ToList();
        
            var NhanViens = _Context.NhanViens;
            var DsIdNhanVien = NhanViens.Select(x => x.Id).ToList();
        
            for (int i = 0; i < soluong; i++)
            {
                _Context.phieuDangKiMuaHangs.Add(new PhieuDangKiMuaHang()
                {
                    Id = ListIdPhieu[i],
                    IdNhanVien = faker.PickRandom(DsIdNhanVien),
                    NgayTao = faker.Date.Between(DateTime.Today.AddMonths(-1), DateTime.Now),
                });
                for (int j = 0; j < faker.Random.Number(5,10); j++)
                {
                    _Context.ChiTietPhieuMuaHangs.Add(new ChiTietPhieuMuaHang()
                    {
                        Id = ListIdCtPhieu[10 *i + j],
                        IdMatHang = faker.PickRandom(DsIdMathang),
                        SoLuong = faker.Random.Number(5,10),
                        IdPhieuDangKiMuaHang = ListIdPhieu[i]
                    });
                }
               
            }
            await _Context.SaveChangesAsync();
        }

        // public async Task SeedPhieuDangKiMuaHang()
        // {
        //     int numberOfRecords = 1000;
        //     List<Guid> listOfPhieuIds = GenerateGuidList(numberOfRecords);
        //     List<Guid> listOfChiTietPhieuIds = GenerateGuidList(numberOfRecords * 10);
        //
        //     IQueryable<MatHang> allMatHangs = _Context.matHangs;
        //     List<Guid> listOfMatHangIds = allMatHangs.Select(x => x.Id).ToList();
        //
        //     IQueryable<NhanVien> allNhanViens = _Context.NhanViens;
        //     List<Guid> listOfNhanVienIds = allNhanViens.Select(x => x.Id).ToList();
        //
        //     for (int i = 0; i < numberOfRecords; i++)
        //     {
        //         PhieuDangKiMuaHang newPhieu = new PhieuDangKiMuaHang
        //         {
        //             Id = listOfPhieuIds[i],
        //             IdNhanVien = faker.PickRandom(listOfNhanVienIds),
        //             NgayTao = faker.Date.Between(DateTime.Today.AddMonths(-1), DateTime.Now),
        //         };
        //         _Context.phieuDangKiMuaHangs.Add(newPhieu);
        //
        //         int numberOfChiTiet = faker.Random.Number(5, 10);
        //         for (int j = 0; j < numberOfChiTiet; j++)
        //         {
        //             ChiTietPhieuMuaHang newChiTiet = new ChiTietPhieuMuaHang
        //             {
        //                 Id = listOfChiTietPhieuIds[10 * i + j],
        //                 IdMatHang = faker.PickRandom(listOfMatHangIds),
        //                 SoLuong = faker.Random.Number(5, 10),
        //                 IdPhieuDangKiMuaHang = listOfPhieuIds[i]
        //             };
        //             _Context.ChiTietPhieuMuaHangs.Add(newChiTiet);
        //         }
        //     }
        //
        //     await _Context.SaveChangesAsync();
        // }
        //
        // private List<Guid> GenerateGuidList(int numberOfGuids)
        // {
        //     List<Guid> result = new List<Guid>();
        //     for (int i = 0; i < numberOfGuids; i++)
        //     {
        //         result.Add(Guid.NewGuid());
        //     }
        //     return result;
        // }















    }
}
