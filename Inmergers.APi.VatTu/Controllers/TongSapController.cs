﻿using System.Threading.Tasks;
using Inmergers.Business.Service.TongSap;
using Inmergers.Common;
using Inmergers.Data.Data.DbContext;
using Microsoft.AspNetCore.Mvc;

namespace Inmergers.APi.VatTu.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TongSapController :ControllerBase
    {
        private readonly ITongSapServices _iTongSapServices;
        private readonly vattudbContext _context;
        public TongSapController(vattudbContext context, ITongSapServices ITongSap)
        {
            _iTongSapServices = ITongSap;
            _context = context;
        }
        [HttpGet, Route("NhieuSanPhamNhat")]
        public async Task<IActionResult> ThangNamDangkiDuocNhieuSanPhamNhat()
        {
            var result = await _iTongSapServices.ThangNamCoNhieuNhieuSanPhamDangkiNhat();
            return Helper.TransformData(result);
        }
        [HttpGet, Route("NhieuNhanVienNhat")]
        public async Task<IActionResult> ThangNamNhieuNhatVienMoi()
        {
            var result = await _iTongSapServices.ThangNamCoNhieuNhieuNhanVienNhat();
            return Helper.TransformData(result);
        }




    }
}
