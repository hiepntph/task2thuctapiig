﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Inmergers.Business.Hubs.Models;
using Inmergers.Business.Service.BoPhan;
using Inmergers.Business.Service.NhanVien;
using Inmergers.Common;
using Inmergers.Common.Utils;
using Inmergers.Data.Data.DbContext;
using Inmergers.Data.Data.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Inmergers.APi.VatTu.Controllers
{
    
   
    [ApiController]
    [Route("api/NhanVien")]
    public class NhanVienController : ControllerBase
    {
        private readonly vattudbContext _vatuVattudbContext;
        private readonly IMapper _mapper;
        private readonly AppSetting _appSettings;
        private readonly INhanVienServices _inhanVienServices;
        public NhanVienController(vattudbContext vattudbContext, IMapper mapper,  IOptionsMonitor<AppSetting> optionsMonitor, INhanVienServices inhaNhanVienServices)
        {
            _vatuVattudbContext = vattudbContext;
            _mapper = mapper;
            _appSettings = optionsMonitor.CurrentValue;
            _inhanVienServices = inhaNhanVienServices;
        }

        [HttpPost, Route("login")]
        public async Task<IActionResult> Login([FromQuery] dangNhapNhanVien model)
        {

            var login = await _inhanVienServices.Login(model);

            return Helper.TransformData(login);
        }

        #region Laytokencu




        // [HttpPost("Login")]
        // public IActionResult Validatebnguoidung([FromQuery] DangNhapModels model)
        // {
        //     var user = _vatuVattudbContext.NhanViens.FirstOrDefault(a => a.TenTk == model.tenDangNhap && model.MatKhau == a.matkhau);
        //
        //     if (user == null)
        //     {
        //         return Ok(new ApiResponse
        //         {
        //             Success = false,
        //             Message = "đăng nhập thất bại",
        //         });
        //     }
        //     return Ok(new ApiResponse
        //     {
        //         Success = true,
        //         Message = "đăng nhập thành công",
        //         Data = GenerateToken(user)
        //     });
        // }
        // // tạo 1 hàm jwt token trả về dưới dạng token model
        // private TokenModel GenerateToken(NhanVien nhanVien)
        // {
        //     //cung cấp các phương thức để xử lý tạo và ghi jwt,
        //     var jwtTokenHandler = new JwtSecurityTokenHandler();
        //     //tạo mã khóa bí mật lấy từ secretkey
        //     var secretkeyBytes = Encoding.UTF8.GetBytes(_appSettings.Secretkey);
        //     // chứa thông tin về người dùng mà token được tạo cho.
        //     var tokenDescription = new SecurityTokenDescriptor
        //     {
        //         Subject =
        //             //tạo mới biểu diễn thông tin người dùng hoặc 1 nhóm người dùng 
        //             new ClaimsIdentity(new[]
        //         {
        //             new Claim(ClaimTypes.Name,nhanVien.Hoten),
        //             new Claim(ClaimTypes.Email,nhanVien.gmail),
        //             new Claim(ClaimTypes.Role,nhanVien.TenTk),
        //             new Claim("PassWord",nhanVien.matkhau),
        //             new Claim("TenTk", nhanVien.TenTk),
        //             new Claim("IdBoPhan", nhanVien.IdBoPhan.ToString()),
        //             new Claim("Id", nhanVien.Id.ToString()),
        //             new Claim("TokenId", Guid.NewGuid().ToString())
        //         }),
        //         Expires = DateTime.UtcNow.AddMinutes(900),
        //         // tạo ra 1 đối tượng để ký token 
        //         // tạo ra 1 đối tượng signing bằng cách sử dụng mã hóa và thuật toán đã chỉ định 
        //         SigningCredentials = new SigningCredentials(
        //             // tạo ra 1 đối tượng với khóa mã được truyền vào là secretkeyBytes
        //             new SymmetricSecurityKey(secretkeyBytes),
        //             //chỉ định thuật toán ký số là HMAC SHA512.
        //             SecurityAlgorithms.HmacSha512Signature)
        //
        //     };
        //
        //     var token = jwtTokenHandler.CreateToken(tokenDescription);
        //     //chuyển đổi một đối tượng  sang dạng chuỗi
        //     var accessToken = jwtTokenHandler.WriteToken(token);
        //
        //     return new TokenModel
        //     {
        //         AccessToken = accessToken,
        //         RefreshToken = GenerateRefreshToken()
        //     };
        // }
        //
        // // tạo 1 đoạn khóa bảo mật với kí tự ngẫu nhiên 
        // private string GenerateRefreshToken()
        // {
        //     var ramdom = new byte[50];
        //     using (var rng = RandomNumberGenerator.Create())
        //     {
        //         rng.GetBytes(ramdom);
        //
        //         return Convert.ToBase64String(ramdom);
        //     }
        // }
        // [HttpPost, Route("")]
        // [ProducesResponseType(typeof(ResponseList<BoPhanModels>), StatusCodes.Status200OK)]
        // public async Task<IActionResult> createBoPhanByLogin([FromQuery] BoPhanCreateModel boPhan)
        // {
        //     //lấy token
        //     var requestInfo = Helper.GetRequestInfo(Request);
        //     var actorId = requestInfo.UserId;
        //     var resule = await _iboPhanServices.CreateByLogin(boPhan);
        //     return Helper.TransformData(resule);
        // }

        #endregion

    }
}
