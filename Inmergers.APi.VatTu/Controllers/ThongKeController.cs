﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Inmergers.Business.Service.ThongKe;
using Inmergers.Common;
using Inmergers.Data.Data.DbContext;
using Inmergers.Data.Data.Entity;
using Microsoft.AspNetCore.Mvc;

namespace Inmergers.APi.VatTu.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ThongKeController : ControllerBase
    {

        private readonly IThongKeServices _IThongkeServices;
        private readonly vattudbContext _context;

        public ThongKeController(vattudbContext context , IThongKeServices ithongkeServices)
        {
            _IThongkeServices = ithongkeServices;
            _context = context;
        }
        [HttpGet, Route("ThongKeNhanVienTheoThang")]
        public async Task<IActionResult> ThongKeNhanVienTheoThang()
        {
            var result = await _IThongkeServices.ThongKeSoLuongNhanVienTheoThang();
            return Helper.TransformData(result);
        }
        [HttpGet, Route("ThongKeSoLuongNhanVienTrongBoPhan")]
        public async Task<IActionResult> ThongKeSoLuongNhanVienTrongBoPhan()
        {
            var result = await _IThongkeServices.ThongKeSoLuongNhanVienTrongBoPhan();
            return Helper.TransformData(result);
        }
        [HttpGet, Route("ThongKeSoLuongSanPhamDangkyTheoThang")]
        public async Task<IActionResult> ThongKeSoLuongSanPhamDangkyTheoThang()
        {
            var result = await _IThongkeServices.ThongKeSoLuongSanPhamDangkyTheoThang();
            return Helper.TransformData(result);
        }
    }
}
