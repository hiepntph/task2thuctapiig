﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Inmergers.Business.Hubs.Models;
using Inmergers.Business.Service.MatHang;
using Inmergers.Business.Service.NguoiDung;
using Inmergers.Common;
using Inmergers.Common.Utils;
using Inmergers.Data.Data.DbContext;
using Inmergers.Data.Data.Entity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
//token là 1 chuỗi đại diện cho đối tượng hoặc thông tin bảo mật
//token bảo mật và token xác thực
namespace Inmergers.APi.VatTu.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/NguoiDung")]
    public class NguoiDungController : ControllerBase
    {
        private readonly vattudbContext _vatuVattudbContext;
        private readonly IMapper _mapper;
        private readonly INguoiDungServices _iNguoiDungServices;
        private readonly AppSetting _appSettings;

        public NguoiDungController(vattudbContext vattudbContext, IMapper mapper, INguoiDungServices iNguoiDungServices, IOptionsMonitor<AppSetting> optionsMonitor)
        {
            _vatuVattudbContext = vattudbContext;
            _mapper = mapper;
            _iNguoiDungServices = iNguoiDungServices;
            _appSettings = optionsMonitor.CurrentValue;
        }
        [HttpPost("Login")]
        public IActionResult Validatebnguoidung([FromQuery] DangNhapModels model)
        {
            var user = _vatuVattudbContext.nguoiDungs.FirstOrDefault(a => a.TenTaiKhoan == model.tenDangNhap && model.MatKhau == a.MatKhau);

            if (user == null)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Message = "Invalid username/password"
                });
            }
            return Ok(new ApiResponse
            {
                Success = true,
                Message = "Authenticate success",
                Data = GenerateToken(user)
            });
        }
        // tạo 1 hàm jwt token trả về dưới dạng token model
        private TokenModel GenerateToken(NguoiDung nguoiDung)
        {
            //cung cấp các phương thức để xử lý tạo và ghi jwt,
            var jwtTokenHandler = new JwtSecurityTokenHandler();
            //tạo mã khóa bí mật lấy từ secretkey
            var secretkeyBytes = Encoding.UTF8.GetBytes(_appSettings.Secretkey);
                // chứa thông tin về người dùng mà token được tạo cho.
            var tokenDescription = new SecurityTokenDescriptor
            {
                Subject =
                    //tạo mới biểu diễn thông tin người dùng hoặc 1 nhóm người dùng 
                    new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name,nguoiDung.HoTen),
                    new Claim(ClaimTypes.Email,nguoiDung.Email),
                    new Claim("PassWord",nguoiDung.MatKhau),
                    new Claim("UseName", nguoiDung.TenTaiKhoan),
                    new Claim("Id", nguoiDung.Id.ToString()),
                    new Claim("TokenId", Guid.NewGuid().ToString())
                }),
                 Expires = DateTime.UtcNow.AddMinutes(900),
                 // tạo ra 1 đối tượng để ký token 
                 // tạo ra 1 đối tượng signing bằng cách sử dụng mã hóa và thuật toán đã chỉ định 
                SigningCredentials = new SigningCredentials(
                    // tạo ra 1 đối tượng với khóa mã được truyền vào là secretkeyBytes
                    new SymmetricSecurityKey(secretkeyBytes),
                    //chỉ định thuật toán ký số là HMAC SHA512.
                    SecurityAlgorithms.HmacSha512Signature)

            };

            var token = jwtTokenHandler.CreateToken(tokenDescription);
            //chuyển đổi một đối tượng  sang dạng chuỗi
            var accessToken = jwtTokenHandler.WriteToken(token);

            return new TokenModel
            {
                AccessToken = accessToken,
                RefreshToken = GenerateRefreshToken()
            };
        }

        // tạo 1 đoạn khóa bảo mật với kí tự ngẫu nhiên 
        private string GenerateRefreshToken()
        {
            var ramdom = new byte[50];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(ramdom);

                return Convert.ToBase64String(ramdom);
            }
        }
        [HttpPost, Route("CreateNguoiDung")]
        [ProducesResponseType(typeof(ResponseList<NguoiDungModels>), StatusCodes.Status200OK)]
        public async Task<IActionResult> createNguoiDung([FromQuery] CreateNguoiDung nguoidung)
        {
            var result = await _iNguoiDungServices.createNguoiDung(nguoidung);
            return Helper.TransformData(result);
        }
        [HttpGet, Route("GetNguois")]
        // [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        // [Authorize]
        public async Task<IActionResult> getBoPhan([FromQuery] NguoiDungQueryModel nguoiDung)
        {
            var result = await _iNguoiDungServices.getNguoiDung(nguoiDung);
            return Helper.TransformData(result);
        }

        [HttpGet]
        [Route("{Id}/GetNguoiDungbyId")]
        // [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        public async Task<IActionResult> getIdBoPhan(Guid Id)
        {
            var result = await _iNguoiDungServices.getIdNguoiDungg(Id);
            return Helper.TransformData(result);
        }

        [HttpPut]
        [Route("UpdateNguoiDung")]
        //[ProducesResponseType(typeof(ResponseList<BoPhanModels>), StatusCodes.Status200OK)]
        public async Task<IActionResult> updateBoPhan(Guid Id, [FromBody] UpdateNguoiDung nguoiDung)
        {
            var resule = await _iNguoiDungServices.updateNguoiDung(Id, nguoiDung);
            return Helper.TransformData(resule);
        }

        [HttpDelete]
        [Route("{Id}/DeleteNguoiDung")]
        public async Task<IActionResult> deleteBoPhan(Guid Id)
        {
            var resule = await _iNguoiDungServices.deleteNguoiDung(Id);
            return Helper.TransformData(resule);
        }




















    }
}
