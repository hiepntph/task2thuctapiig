﻿using System.Threading.Tasks;
using DotNetCore.CAP.Messages;
using Inmergers.Common;
using Microsoft.AspNetCore.Mvc;


namespace Inmergers.APi.VatTu.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SeeddingController
    {
        private FakeData.Seedding _seedding;
       
        public SeeddingController(FakeData.Seedding seedding)
        {
            this._seedding = seedding;
        }


        [HttpGet]
        [Route("Seedding")]
        public async Task<string> FakeToanbo()
        {
            await _seedding.SeeddingBoPhan();
            await _seedding.SeeddingNhanVien();
            await _seedding.SeeddingSanPham();
            await _seedding.SeeddingPhieuDangKiMuahang();
            return "Thành Công";
        }
    }
}
