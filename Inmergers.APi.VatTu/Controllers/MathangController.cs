﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Inmergers.Business.Service.BoPhan;
using Inmergers.Business.Service.MatHang;
using Inmergers.Common;
using Inmergers.Common.Utils;
using Inmergers.Data.Data.DbContext;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Inmergers.APi.VatTu.Controllers
{

    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/MatHang")]
    public class MathangController : ControllerBase
    {
        private readonly vattudbContext _vatuVattudbContext;
        private readonly IMapper _mapper;
        private readonly IMatHangServices _IMatHangServices;

        public MathangController(vattudbContext vattudbContext, IMapper mapper, IMatHangServices IMatHangServices)
        {
            _vatuVattudbContext = vattudbContext;
            _mapper = mapper;
            _IMatHangServices = IMatHangServices;
        }

        [HttpGet, Route("GetMatHang")]
        public async Task<IActionResult> getMatHang([FromQuery] MatHangQueryModel matHang)
        {
            var matHangRepo = await _IMatHangServices.getMatHang(matHang);
            return Helper.TransformData(matHangRepo);
        }

        [HttpGet, Route("{Id}/GetMatHangbyId")]
        public async Task<IActionResult> getIdMatHang(Guid Id)
        {
            var result = await _IMatHangServices.getIdMatHang(Id);
            return Helper.TransformData(result);
        }

        [HttpPost, Route("CreateMatHang")]
        public async Task<IActionResult> createMatHang([FromQuery] MatHangCreateModels matHang)
        {
            var result = await _IMatHangServices.createMatHang(matHang);
            return Helper.TransformData(result);
        }

        [HttpPut, Route("UpdateMatHang")]
        public async Task<IActionResult> updateMatHang(Guid Id, [FromBody] MatHangUpdateModels matHang)
        {
            var result = await _IMatHangServices.updateMatHang(Id, matHang);
            return Helper.TransformData(result);
        }

        [HttpDelete, Route("{Id}/DeleteMatHang")]
        public async Task<IActionResult> deleteMatHang(Guid Id)
        {
            var result = await _IMatHangServices.deleteMatHang(Id);
            return Helper.TransformData(result);
        }



    }
}
