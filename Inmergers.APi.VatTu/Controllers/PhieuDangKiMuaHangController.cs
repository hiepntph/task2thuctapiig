﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Inmergers.Business.Service.BoPhan;
using Inmergers.Business.Service.PhieuDangKiMuaHang;
using Inmergers.Common;
using Inmergers.Common.Utils;
using Inmergers.Data.Data.DbContext;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Inmergers.APi.VatTu.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/phieudangkimuahangs")]
    [ApiController]
    public class PhieuDangKiMuaHangController : ControllerBase
    {
        private readonly vattudbContext _vatuVattudbContext;
        private readonly IMapper _mapper;
        private readonly IPhieuDangKiMuaHangServices _iDangKiMuaHangServices;

        public PhieuDangKiMuaHangController(vattudbContext vattudbContext, IMapper mapper, IPhieuDangKiMuaHangServices IDangKiMuaHangServices)
        {
            _vatuVattudbContext = vattudbContext;
            _mapper = mapper;
            _iDangKiMuaHangServices = IDangKiMuaHangServices;
        }

        
        [HttpGet, Route("GetPhieuDangKiMuaHang")]
        public async Task<IActionResult> getPhieuDangKiMuaHang([FromQuery] PhieuDangKiQueryModel boPhan)
        {
            var boPhanRepo = await _iDangKiMuaHangServices.getPhieuDangKiMuaHangg(boPhan);
            return Helper.TransformData(boPhanRepo);
        }

        [HttpGet, Route("{Id}/GetPhieuDangKiMuaHangbyId")]
        public async Task<IActionResult> getIdBoPhan(Guid Id)
        {
            var result = await _iDangKiMuaHangServices.getIPhieuDangKiMuaHangg(Id);
            return Helper.TransformData(result);
        }

        [HttpPost, Route("CreatePhieuDangKiMuaHang")]
        public async Task<IActionResult> createPhieuDangKiMuaHang(PhieuDangKiCreateModel pdk)
        {
            // var identity = HttpContext.User.Identity as ClaimsIdentity;
            // if (identity != null)
            // {
            //     var id = identity.FindFirst("Id").Value;
            //     pdk.IdNhanVien = Guid.Parse(id);
            // }
            // var resule = await _iDangKiMuaHangServices.createPhieuDangKiMuaHang(pdk);
            // return Helper.TransformData(resule);
            var resule = await _iDangKiMuaHangServices.createPhieuDangKiMuaHang(pdk);
             return Helper.TransformData(resule);
        }

        [HttpPost, Route("CreatePhieuDangKiMuaHangn")]
        public async Task<IActionResult> createPhieuDangKiMuaHang([FromBody] PhieuDangKiCreateModel[] pdks)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                var id = identity.FindFirst("Id").Value;
                foreach (var pdk in pdks)
                {
                    pdk.IdNhanVien = Guid.Parse(id);
                }
            }
            var resule = await _iDangKiMuaHangServices.createPhieuDangKiMuaHang(pdks);
            return Helper.TransformData(resule);
        }

        [HttpPut, Route("UpdatePhieuDangKiMuaHang")]
        public async Task<IActionResult> updatePhieuDangKiMuaHang(Guid Id, [FromQuery] PhieuDangKiUpdateModel pdk)
        {
            var resule = await _iDangKiMuaHangServices.updatePhieuDangKiMuaHangg(Id, pdk);
            return Helper.TransformData(resule);
        }

        [HttpDelete, Route("{Id}/DeletePhieuDangKiMuaHang")]
        public async Task<IActionResult> deletePhieuDangKiMuaHang(Guid Id)
        {
            var resule = await _iDangKiMuaHangServices.deletePhieuDangKiMuaHang(Id);
            return Helper.TransformData(resule);
        }



    }
}
