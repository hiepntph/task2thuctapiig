﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Inmergers.Business.Service.BoPhan;
using Inmergers.Common;
using Inmergers.Common.Utils;
using Inmergers.Data.Data.DbContext;
using Inmergers.Data.Data.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Inmergers.APi.VatTu.Controllers
{
    //[Authorize(Roles = "hiep")]
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/BoPhan")]
    //[ApiExplorerSettings(GroupName = "BoPhan")]
    public class BoPhanController : ControllerBase
    {
        private readonly vattudbContext _vatuVattudbContext;
        private readonly IMapper _mapper;
        private readonly IBoPhanServices _iBoPhanServices;

        public BoPhanController(vattudbContext vattudbContext, IMapper mapper, IBoPhanServices iBoPhanServices)
        {
            _vatuVattudbContext = vattudbContext;
            _mapper = mapper;
            _iBoPhanServices = iBoPhanServices;
        }

        [ HttpGet, Route("GetBoPhans")]
        // [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        
        public async Task<IActionResult> getBoPhan([FromQuery] BoPhanQueryModel boPhan)
        {
            var boPhanRepo = await _iBoPhanServices.getBoPhan(boPhan);
            return Helper.TransformData(boPhanRepo);
        }

        [HttpGet]
        [Route("{Id}/GetBoPhanbyId")]
        // [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        public async Task<IActionResult> getIdBoPhan(Guid Id)
        {
            var result = await _iBoPhanServices.getIdBoPhan(Id);
            return Helper.TransformData(result);
        }

        [HttpPost, Route("CreateBoPhan")]
        //[ProducesResponseType(typeof(ResponseList<BoPhanModels>), StatusCodes.Status200OK)]
        public async Task<IActionResult> createBoPhan([FromBody] BoPhanCreateModel boPhan)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                var username = identity.FindFirst("TenTk").Value;
                var idbophan = identity.FindFirst("IdBoPhan").Value;
                boPhan.tenNguoiDungDau = username;
                boPhan.tenBoPhan = idbophan;
            }
            var result = await _iBoPhanServices.createBoPhan(boPhan);
            return Helper.TransformData(result);
        }

        [HttpPut]
        [Route("Updatebophan")]
        [ProducesResponseType(typeof(ResponseList<BoPhanModels>), StatusCodes.Status200OK)]
        public async Task<IActionResult> updateBoPhan(Guid Id, [FromQuery] BoPhanUpdateModel boPhan)
        {
            var resule = await _iBoPhanServices.updateBoPhan(Id, boPhan);
            return Helper.TransformData(resule);
        }

        [HttpDelete]
        [Route("{Id}/Deletebophans")]
        public async Task<IActionResult> deleteBoPhan(Guid Id)
        {
            var resule = await _iBoPhanServices.deleteBoPhan(Id);
            return Helper.TransformData(resule);
        }



















    }
}
