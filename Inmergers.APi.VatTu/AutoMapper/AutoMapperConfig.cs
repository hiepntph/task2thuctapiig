﻿using AutoMapper;
using Inmergers.Business.Service.BoPhan;
using Inmergers.Business.Service.MatHang;
using Inmergers.Business.Service.NguoiDung;
using Inmergers.Business.Service.PhieuDangKiMuaHang;
using Inmergers.Common.Utils;
using Inmergers.Data.Data.Entity;

namespace Inmergers.APi.VatTu.AutoMapper
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<boPhan, BoPhanModels>().ReverseMap();
            CreateMap<MatHang, MatHangModels>().ReverseMap();
           

            #region phieudangki
            CreateMap<PhieuDangKiMuaHang, PhieuDangKiMuaHangModels>().ReverseMap();
            CreateMap<PhieuDangKiMuaHang, PhieuDangKiCreateModel>().ReverseMap();

            CreateMap<ChiTietPhieuDangKiCreate, ChiTietPhieuMuaHang>();
            CreateMap<Pagination<PhieuDangKiMuaHang>, Pagination<PhieuDangKiMuaHangModels>>();
            CreateMap<PhieuDangKiCreateModel, PhieuDangKiMuaHang>().ForMember(dest => dest.ChiTietPhieuMuaHang, opt => opt.MapFrom(src => src.MatHangCreate));
            CreateMap<ChiTietPhieuMuaHang, ChiTietPhieuDangKiModel>();
            #endregion


        }
    }
}
