﻿

using System;
using System.Collections.Generic;

namespace Inmergers.Common
{
    public static class Constant
    {
        public static class MqttMessageType
        {
            public const string UpdateTransaction = "SYNC_TRANSACTION_MSG";
            public const string UpdateIpCamera = "CAMERA_STATUS";
        }
    }

    public class RegisterCodeConstants
    {
        public static class RegisterCodeStatus
        {
            public const string USED = "USED";
            public const string UN_USED = "UN_USED";
        }
    }

    #region Post

    public static class PostConstant
    {
        public static class PublishStatus
        {
            public const string DRAFT = "DRAFT";
            public const string PUBLISHED = "PUBLISHED";
        }
    }

    #endregion Post

    public class UserConstants
    {
        public static Guid AdministratorId => new Guid("00000000-0000-0000-0000-000000000001");
        public static Guid UserId => new Guid("00000000-0000-0000-0000-000000000002");
        public static Guid VnPost => new Guid("00000000-0000-0000-0000-000000000003");
        public static int IsSuperAdmin = 999;
        public static int IsUser = 0;
        public static int IsStaff = 1;
    }

    public class RoleConstants
    {
        public static Guid AdministratorId => new Guid("00000000-0000-0000-0000-000000000001");

        public static Guid StaffId => new Guid("0c97fade-a380-4c97-ba92-f53c93c5ed82");
        public static Guid ContributorLv1 => new Guid("a0aba7cc-4fe2-451d-8e6a-f285b5137b1b");
        public static Guid ContributorLv2 => new Guid("ac9f9eb7-1d71-43d3-b3af-7e2bd091f23a");
        public static Guid ContributorLv3 => new Guid("6ddbb2ce-d720-401e-9950-25917aef973f");

        public const string AdministratorCode = "ADMIN";
        public const string StaffCode = "STAFF";
        public const string CONTRIBUTOR_LV1_Code = "CONTRIBUTOR_LV1";
        public const string CONTRIBUTOR_LV2_Code = "CONTRIBUTOR_LV2";
        public const string CONTRIBUTOR_LV3_Code = "CONTRIBUTOR_LV3";
    }

    public class RightConstants
    {
        public static Guid AccessAppId => new Guid("00000000-0000-0000-0000-000000000001");
        public static string AccessAppCode = "TRUY_CAP_HE_THONG";
        public static Guid DefaultAppId => new Guid("00000000-0000-0000-0000-000000000002");
        public static string DefaultAppCode = "TRUY_CAP_MAC_DINH";
        public static string VIEW = "VIEW";
    }

    public class NavigationConstants
    {
        // QUẢN TRỊ HỆ THỐNG
        public static Guid SystemNav => new Guid("00000000-0000-0000-0000-000000000001");

        // Quản lý nhóm
        public static Guid RoleNav => new Guid("00000000-0000-0000-0000-000000000011");

        // Quản lý quyền
        public static Guid RightNav => new Guid("00000000-0000-0000-0000-000000000021");

        // Quản lý người dùng
        public static Guid UserNav => new Guid("00000000-0000-0000-0000-000000000031");

        // PHÂN QUYỀN
        public static Guid PermissionNav => new Guid("00000000-0000-0000-0000-000000000002");

        // Phân quyền điều hướng
        public static Guid NavNav => new Guid("00000000-0000-0000-0000-000000000012");

        // Phân quyền người dùng
        public static Guid RMUNav => new Guid("00000000-0000-0000-0000-000000000022");
    }

    public class AppConstants
    {
        public static string EnvironmentName = "production";
        public static Guid HO_APP => new Guid("00000000-0000-0000-0000-000000000001");
    }

    public static class ParamConstants
    {
        public const string VNPOST_DEFAULT_SHIPPING_PRICE = "VNPOST_DEFAULT_SHIPPING_PRICE";
        public const string VNPOST_TEST_ENABLED = "VNPOST_TEST_ENABLED";
        public const string SHIPPING_DEFAULT_COMPANY = "SHIPPING_DEFAULT_COMPANY";
        public const string SHOPEE_SHIPPING_CARRIERS = "SHOPEE_SHIPPING_CARRIERS";
        public const string VNPOST_MAX_FAST_DELIVERY_WEIGHT = "VNPOST_MAX_FAST_DELIVERY_WEIGHT";

        public const string VIETTEL_DEFAULT_SHIPPING_PRICE = "VIETTEL_DEFAULT_SHIPPING_PRICE";
        public const string VIETTEL_TEST_ENABLED = "VIETTEL_TEST_ENABLED";
        public const string VIETTEL_MAX_FAST_DELIVERY_WEIGHT = "VIETTEL_MAX_FAST_DELIVERY_WEIGHT";
        public const string VIETTEL_SENDER_PROVINCE_ID = "VIETTEL_SENDER_PROVINCE_ID";
        public const string VIETTEL_SENDER_DISTRICT_ID = "VIETTEL_SENDER_DISTRICT_ID";
        public const string VIETTEL_SECRET_KEY = "VIETTEL_SECRET_KEY";
        public const string MAINTENANCE_ORDER_CREATE = "MAINTENANCE_ORDER_CREATE";
        public const string PAYMENT_REQUEST_INVOICE_FEE = "PAYMENT_REQUEST_INVOICE_FEE";
        public const string REGISTER_CODE_TIMEOUT = "REGISTER_CODE_TIMEOUT";
        public const string REGISTER_CODE_ENABLED = "REGISTER_CODE_ENABLED";

        public const string WALLET_DEPOSIT_SUGGESTED_AMOUNTS = "WALLET_DEPOSIT_SUGGESTED_AMOUNTS";
        public const string WALLET_DEPOSIT_MIN_AMOUNT = "WALLET_DEPOSIT_MIN_AMOUNT";

        public const string WALLET_DEPOSIT_BANK_NUMBER = "WALLET_DEPOSIT_BANK_NUMBER";
        public const string WALLET_DEPOSIT_BANK_NAME = "WALLET_DEPOSIT_BANK_NAME";
        public const string WALLET_DEPOSIT_BANK_OWNER = "WALLET_DEPOSIT_BANK_OWNER";

        public const string VERSION_OLD_IOS = "VERSION_OLD_IOS";
        public const string VERSION_OUTDATED_IOS = "VERSION_OUTDATED_IOS";
        public const string VERSION_OLD_ANDROID = "VERSION_OLD_ANDROID";
        public const string VERSION_OUTDATED_ANDROID = "VERSION_OUTDATED_ANDROID";

        public const string MAINTENANCE_TOP_ANNOUNCEMENT = "MAINTENANCE_TOP_ANNOUNCEMENT";
    }

    public static class CodeTypeConstants
    {
        public const string FranchiseType = "FranchiseType";
        public const string Industry = "Industry";
        public const string InvestmentType = "InvestmentType";
        public const string InvestorRole = "InvestorRole";
        public const string LegalEntityType = "LegalEntityType";
        public const string SellerDemand = "SellerDemand";
        public const string BusinessRole = "BusinessRole";
    }
    public class ProfileTypeConstants
    {
        public const string Seller = "SELLER";
        public const string Buyer = "BUYER";
        public const string Franchise = "FRANCHISE";
    }

    public class ProfileStatus
    {
        public const string Draft = "DRAFT";
        public const string Pending = "PENDING";
        public const string Approved = "APPROVED";
    }

    public enum ProfileType
    {
        SELLER = 1,
        BUYER = 2,
        FRANCHISE = 3
    }

    public static class PaymentRequestConstants
    {
        public const string Status_Reject = "REJECT"; // Hủy 
        public const string Status_WaitConfirm = "WAIT_CONFIRM"; // Chờ duyệt
        public const string Status_BankTransfer = "BANK_TRANSFER"; // Chờ duyệt
        public const string Status_Completed = "COMPLETED";
    }
    public enum InvoiceStatus
    {
        INITIALIZATION = 1,
        PAYMENT_PAID = 2,
        PAYMENT_UNPAID = 3
    }

    public class PaymentEnum
    {
        public enum PackageDetailStatus
        {
            NotActivated,
            Activated,

        }
        public static class AccountStroageUse
        {
            public static bool NotUsedYed = false;
            public static bool Used = true;
        }

        public class Stripe
        {
            public static List<string> zeroDecimalCurrency = new List<string> { "BIF", "CLP", "DJF", "GNF", "JPY", "KMF", "KRW", "MGA", "PYG", "RWF", "UGX", "VND", "VUV", "XAF", "XOF", "XPF" };


            public class Metadata
            {
                public static string ProfileId = "ProfileId";
                public static string AccountId = "AccountId";
                public static string InvoiceId = "InvoiceId";
                public static string PackageId = "ServicePackageId";
                public static string ServiceIndividualIds = "ServiceIndividualIds";
                public static string EventId = "EventId";
                public static string ItemType = "ItemType";
            }
        }
    }

    public class PackageEnum
    {
        public enum PackageType
        {
            Free_Plan = 1,
            Silver_Plan = 2,
            Gold_Plan = 3,
            Platinum_Plan = 4
        }

    }
    public class ItemTypeEnum
    {
        public const int SERVICE_PACKAGE = 1;
        public const int SERVICE_INDIVIDUAL = 2;
        public const int EVENT_ENTRY = 3;
    }

    public class PackageConstants
    {
        public const int BUSINESS_BASIC = 5;
        public const int BUSINESS_SILVER = 6;
        public const int BUSINESS_GOLD = 7;
        public const int BUSINESS_PLATINUM = 8;
        public const int FRANCHISE_BASIC = 5;
        public const int FRANCHISE_SILVER = 6;
        public const int FRANCHISE_GOLD = 7;
        public const int FRANCHISE_PLATINUM = 8;
        public const int INVESTOR_BASIC = 9;
        public const int INVESTOR_SILVER = 10;
        public const int INVESTOR_GOLD = 11;
        public const int INVESTOR_PLATINUM = 12;
    }

    public class PackageIndividualConstants
    {
        public const int FASTTRACK_BUNDLE = 1;
        public const int STAY_PRIVATE = 2;
        public const int HIRE_ACCOUNT_MANAGER = 3;
        public const int PREPARE_IM_BUSINESS_PLAN = 4;
        public const int GET_A_BUSINESS_VALUATION = 5;
        public const int UNLIMITED_PROPOSALS = 6;
        public const int HIRE_ACCOUNT_MANAGER_1 = 7;
        public const int UNLIMITED_PROPOSALS_1 = 8;
        public const int HIRE_ACCOUNT_MANAGER_2 = 9;
        public const int FASTTRACK_BUNDLE_1 = 8;
    }
    public enum CommissionStatus
    {
        NOT_CALCULATED = 1,
        CALCULATED = 2
    }

    public class EventTypeEnum
    {
        public const int SEND_PROPOSAL = 1;
        public const int SEND_INTRODUCTION = 2;
        public const int VIEW_PROFILE = 3;
        public const int PROFILE_APPROVE = 4;
    }
}