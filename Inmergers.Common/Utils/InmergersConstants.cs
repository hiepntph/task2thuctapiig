﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inmergers.Common.Utils
{
    public class InmergersConstants
    {
        public static class CommandTypes
        {
            public const string Push = "PUSH_CMD";
            public const string Pull = "PULL_CMD";
        }

        public static class ClaimConstants
        {
            public static string USER_ID = "x-userId";
            public static string APP_ID = "x-appId";
            public static string USER_NAME = "x-userName";
            public static string FULL_NAME = "x-fullName";
            public static string ROLES = "x-roles";
            public static string RIGHTS = "x-roles";
            public static string LEVEL = "x-level";
            public static string ISSUED_AT = "x-iat";
            public static string EXPIRES_AT = "x-exp";
            public static string CHANNEL = "x-channel";
            public static string PHONE = "x-phone";
            public static string LANGUAGE = "x-language";
            public static string CURRENCY = "x-currency";
        }

        public static class LanguageConstants
        {
            public const string Default = "en";
            public const string English = "en";
            public const string Vietnamese = "vn";
            public const string Japanese = "jp";
            public const string Chinese = "zh";
        }

        public static class CurrencyConstants
        {
            public const string Default = "USD";
        }

        public static class AppChanel
        {
            public static int WebAdmin = 1;
            public static int Mobile = 0;
        }

        public static class UserRole
        {
            public const string SUPER_ADMIN = "SYS_ADMIN";
            public const string ADMIN = "ADMIN";
            public const string STAFF = "STAFF";
            public const string USER = "USER";
        }

        public static Dictionary<string, int> RoleLevelDict = new Dictionary<string, int>()
        {
            { "SYS_ADMIN", 100 },
            { "ADMIN", 10 },
            { "STAFF ", 9 },
            { "USER", 1 }
        };

        public static Dictionary<string, Guid> RoleIdDict = new Dictionary<string, Guid>()
        {
            { "SYS_ADMIN", new Guid("00000000-0000-0000-0000-000000000099") },
            { "ADMIN", new Guid("00000000-0000-0000-0000-000000000001")  },
            { "STAFF ", new Guid("00000000-0000-0000-0000-000000000002") },
            { "USER", new Guid("00000000-0000-0000-0000-000000000003")  }
        };

        public static class PaymentRequestConstants
        {
            public const string Status_Reject = "REJECT"; // Hủy
            public const string Status_WaitConfirm = "WAIT_CONFIRM"; // Chờ duyệt
            public const string Status_BankTransfer = "BANK_TRANSFER"; // Chờ duyệt
            public const string Status_Completed = "COMPLETED";
        }

        public static class ProfileInteractionConstants
        {
            public const string View = "VIEW";
            public const string Share = "DOWNLOAD";
            public const string Download = "DOWNLOAD";
            public const string Bookmark = "BOOKMARK";
            public const string Connect = "CONTACT";
        }

        public static class FaqConstants
        {
            public const string GENERAL = "GENERAL";
            public const string FRANCHISE_GUIDE = "FRANCHISE_GUIDE";
            public const string SELLER_GUIDE = "SELLER_GUIDE";
            public const string BUYER_GUIDE = "BUYER_GUIDE";
        }
    }
}
