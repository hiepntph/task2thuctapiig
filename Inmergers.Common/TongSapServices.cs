﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Inmergers.Common.Utils;
using Inmergers.Data.Data.DbContext;
using Serilog;

namespace Inmergers.Business.Service.TongSap
{
    public class TongSapServices : ITongSapServices
    {
        private readonly vattudbContext _Context;
        public TongSapServices(vattudbContext context)
        {
            _Context = context;
        }



        public async Task<Response> PBDangKiNhieuSanPhamNhat()
        {
            throw new NotImplementedException();
        }

        public Task<Response> SpDangKiNhieuNhat()
        {
            throw new NotImplementedException();
        }

        public async Task<Response> ThangNamCoNhieuNhieuNhanVienNhat()
        {
            var result = new NamThangSoluongModels();

            var DsNV = _Context.NhanViens
                .GroupBy(x => new { x.ngaytao.Year, x.ngaytao.Month })
                .Select(g => new
                {
                    Month = g.Key.Month,
                    Year = g.Key.Year,
                    soluong = g.Count()
                })
                .OrderByDescending(x => x.soluong)
                .FirstOrDefault();

            if (DsNV != null)
            {
                result.thang = DsNV.Month;
                result.nam = DsNV.Year;
                result.soluong = DsNV.soluong;
            }
            return new ResponseObject<NamThangSoluongModels>(result);
        }

        public async Task<Response> ThangNamCoNhieuNhieuSanPhamDangkiNhat()
        {
            var purchaseOrders = _Context.phieuDangKiMuaHangs
                .Include(x => x.ChiTietPhieuMuaHang)
                .ToList();
            var result = purchaseOrders
                .GroupBy(x => new { x.NgayTao.Year, x.NgayTao.Month })
                .Select(x => new ThangNamNhieuSanPhamNhat
                {
                    thang = x.Key.Month,
                    nam = x.Key.Year,
                    soluong = x.Sum(p => p.ChiTietPhieuMuaHang.Sum(s => s.SoLuong)),
                    Soluongten = x.SelectMany(p => p.ChiTietPhieuMuaHang)
                        .GroupBy(s => _Context.matHangs
                            .Find(s.IdMatHang).tenMatHang)
                        .Select(s => new soLuongTen
                        {
                            Ten = s.Key,
                            soLuong = s.Sum(a=>a.SoLuong)
                        })
                        .ToList()
                })
                .OrderByDescending(x => x.soluong)
                .First();
            
            return new ResponseObject<ThangNamNhieuSanPhamNhat>(result);
        }
    }
}
