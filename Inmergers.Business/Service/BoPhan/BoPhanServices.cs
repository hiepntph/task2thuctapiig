﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Bogus;
using Inmergers.Common.Utils;
using Inmergers.Data.Data.DbContext;
using Inmergers.Data.Data.Entity;
using LinqKit;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Serilog;
using System.Security.Claims;
using Inmergers.Common;
using Code = System.Net.HttpStatusCode;

namespace Inmergers.Business.Service.BoPhan
{
    public class BoPhanServices : IBoPhanServices
    {
        private readonly vattudbContext _Context;
        private readonly IMapper _Mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public BoPhanServices(vattudbContext context, IMapper mapper, IHttpContextAccessor httpContextAccessor)
        {
            _Context = context ?? throw new ArgumentNullException(nameof(context));
            _Mapper = mapper ?? throw new ArgumentNullException(nameof(mapper)); ;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<Response> createBoPhan(BoPhanCreateModel model)
        {
            try
            {
                var entityModel = new boPhan()
                {
                    //IdNhanVien = model.IdNhanVien,
                    Stt = model.Stt,
                    ngayTao = DateTime.Now,
                    tenBoPhan = model.tenBoPhan,
                    tenNguoiDungDau = model.tenNguoiDungDau,
                };
                _Context.Add(entityModel);
                var status = await _Context.SaveChangesAsync();
                if (status > 0)
                {
                    var data = _Mapper.Map<boPhan, BoPhanModels>(entityModel);
                    return new ResponseObject<BoPhanModels>(data, "Thêm thành công");
                }
                return new ResponseError(Code.NotFound, "Thêm thất bại");

            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);

            }
        }

        public async Task<Response> updateBoPhan(Guid Id, BoPhanUpdateModel model)
        {
            try
            {
                var entityModel = await _Context.boPhans.Where(c => c.Id == Id).FirstOrDefaultAsync();
                if (entityModel == null)
                {
                    return new ResponseError(Code.NotFound, "không tìm thấy Id trong bộ phận");
                }

                entityModel.ngayTao = DateTime.Now;
                entityModel.Stt = model.Stt;
                entityModel.tenBoPhan = model.tenBoPhan;
                entityModel.tenNguoiDungDau = model.tenNguoiDungDau;
                var status = await _Context.SaveChangesAsync();
                if (status > 0)
                {
                    var data = _Mapper.Map<boPhan, BoPhanModels>(entityModel);
                    return new ResponseObject<BoPhanModels>(data, "Sửa thành công");
                }
                return new ResponseError(Code.NotFound, "Sửa thất bại");
            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);
            }
        }
        public async Task<Response> getBoPhan(BoPhanQueryModel filter)
        {
            try
            {
                var predicate =  BuildQueryBoPhan(filter);
                var result = _Context.boPhans.Where(predicate).GetPage(filter);
                var boPhanDTo =
                    JsonConvert.DeserializeObject<Pagination<BoPhanModels>>(JsonConvert.SerializeObject(result));
                return new ResponsePagination<BoPhanModels>(boPhanDTo);

            }
            catch (Exception e) 
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);
            }
        }

        public async Task<Response> getIdBoPhan(Guid BpId)
        {
            try
            {
                var entity = await _Context.boPhans.Where(c => c.Id == BpId).FirstOrDefaultAsync();
                if (entity == null)
                {
                    return new ResponseError(Code.NotFound, "Không tìm thấy Id Bộ Phận");
                }

                var data = _Mapper.Map<boPhan, BoPhanModels>(entity);
                return new ResponseObject<BoPhanModels>(data);
            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);
            }
        }

        public async Task<Response> CreateByLogin(BoPhanCreateModel createByLogin)
        {
            try
            {
                var currentUser = System.Threading.Thread.CurrentPrincipal as ClaimsPrincipal;
                var userId = currentUser?.FindFirst(ClaimTypes.NameIdentifier)?.Value;

                if (userId != null)
                {
                    return new ResponseError(Code.NotFound, "Không tìm thấy gì login vừa đăng nhập");
                }
                    var entityModel = new boPhan()
                    {
                        Stt = createByLogin.Stt,
                        ngayTao = DateTime.Now,
                        tenBoPhan = userId,
                        tenNguoiDungDau = userId,
                        //IdNhanVien = Guid.Parse(userId)
                    };
                    _Context.Add(entityModel);
                    var status = await _Context.SaveChangesAsync();
                    if (status > 0)
                    {
                        var data = _Mapper.Map<boPhan, BoPhanModels>(entityModel);
                        return new ResponseObject<BoPhanModels>(data, "Thêm thành công");
                    }
                
                return new ResponseError(Code.NotFound, "Thêm thất bại");
            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);
            }
        }

        public async Task<Response> deleteBoPhan(Guid BpId)
        {
            try
            {
                var entity =  await _Context.boPhans.Where(c=>c.Id == BpId).FirstOrDefaultAsync();
                if (entity == null)
                {
                    return new ResponseError(Code.NotFound, "Không tìm thấy Id Bộ Phận");
                }
                _Context.Remove(entity);
                _Context.SaveChanges();
                var tile = entity.tenBoPhan;
                return new ResponseDelete(Code.OK, "Xóa thành công", BpId, tile);

            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);
            }
        }
        private Expression<Func<boPhan, bool>> BuildQueryBoPhan(BoPhanQueryModel query)
        {
            var predicate = PredicateBuilder.New<boPhan>(true);

            if (query.BoPhanId.HasValue && query.BoPhanId.Value != Guid.Empty)
            {
                predicate = predicate.And(c => c.Id == query.BoPhanId);
            }

            if (!string.IsNullOrEmpty(query.FullTextSearch))
                predicate.And(c => c.tenBoPhan.Contains(query.FullTextSearch)
                                   || c.tenNguoiDungDau.Contains(query.FullTextSearch));

            return predicate;
        }
        
    }
}
