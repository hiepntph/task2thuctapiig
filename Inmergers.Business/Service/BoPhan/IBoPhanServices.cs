﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Inmergers.Common.Utils;

namespace Inmergers.Business.Service.BoPhan
{
    public interface IBoPhanServices
    {
        Task<Response> createBoPhan(BoPhanCreateModel model);
        Task<Response> updateBoPhan(Guid Id, BoPhanUpdateModel model);
        Task<Response> getBoPhan(BoPhanQueryModel filter);
        Task<Response> getIdBoPhan(Guid BpId);
        Task<Response> deleteBoPhan(Guid BpId);
        Task<Response> CreateByLogin(BoPhanCreateModel createByLogin);
    }
}
