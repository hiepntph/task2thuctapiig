﻿using System;
using System.Collections.Generic;
using System.Text;
using Inmergers.Common.Utils;
using Newtonsoft.Json;

namespace Inmergers.Business.Service.BoPhan
{
    public class BoPhanModels
    {
        //loại bỏ 1 thuộc tính không cho hiện
        [JsonIgnore]
        public Guid Id { get; set; }
        public int Stt { get; set; }
        public string tenNguoiDungDau { get; set; }
        public string tenBoPhan { get; set; }
        public DateTime ngayTao { get; set; }
    }

    public class BoPhanCreateModel
    {
        public int Stt { get; set; }
        public string tenNguoiDungDau { get; set; }
        public string tenBoPhan { get; set; }
        public DateTime ngayTao { get; set; }
    }
    public class BoPhanUpdateModel
    {
        public int Stt { get; set; }
        public string tenNguoiDungDau { get; set; }
        public string tenBoPhan { get; set; }
        public DateTime ngayTao { get; set; }
      
    }
    public class BoPhanQueryModel : PaginationRequest
    {
        public Guid? BoPhanId { get; set; }
    }
    public class createByLogin
    {
        public int Stt { get; set; }
       
    }


}
