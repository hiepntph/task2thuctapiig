﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Inmergers.Common.Utils;

namespace Inmergers.Business.Service.TongSap
{
    public interface ITongSapServices
    {
        Task<Response> PBDangKiNhieuSanPhamNhat();
        Task<Response> SpDangKiNhieuNhat();
        Task<Response> ThangNamCoNhieuNhieuNhanVienNhat();
        Task<Response> ThangNamCoNhieuNhieuSanPhamDangkiNhat();
    }
}
