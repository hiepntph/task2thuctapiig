﻿using System;
using System.Collections.Generic;
using System.Text;
using Inmergers.Data.Data.Entity;

namespace Inmergers.Business.Service.TongSap
{
    public class ThangNamNhieuSanPhamNhat
    {
        public int nam { get; set; }
        public int thang { get; set; }
        public int soluong { get; set; }
        public List<soLuongTen> Soluongten { get; set; }
    }

    public class soLuongTen
    {
        public string Ten { get; set; }
        public int soLuong { get; set; }
    }

    public class NamThangSoluongModels
    {
        public int nam { get; set; }
        public int thang { get; set; }
        public int soluong { get; set; }
    }
}
