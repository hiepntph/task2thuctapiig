﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Inmergers.Business.Service.MatHang;
using Inmergers.Common.Utils;
using Inmergers.Data.Data.DbContext;
using Serilog;
using Code = System.Net.HttpStatusCode;
using LinqKit;
using System.Linq;
using System.Linq.Expressions;
using Inmergers.Business.Service.BoPhan;
using Inmergers.Common;
using Inmergers.Data.Data.Entity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Inmergers.Business.Service.PhieuDangKiMuaHang
{
    public class PhieuDangKiMuaHangServices : IPhieuDangKiMuaHangServices
    {
        private readonly vattudbContext _Context;
        private readonly IMapper _Mapper;

        public PhieuDangKiMuaHangServices(vattudbContext context, IMapper mapper)
        {
            _Context = context ?? throw new ArgumentNullException(nameof(context));
            _Mapper = mapper ?? throw new ArgumentNullException(nameof(mapper)); ;
        }
        public async Task<Response> createPhieuDangKiMuaHang(PhieuDangKiCreateModel model)
        {
            try
            {
                // var entity = new Data.Data.Entity.PhieuDangKiMuaHang()
                //     {
                //       
                //         IdNhanVien = model.IdNhanVien,
                //     };
                // _Context.Add(entity);
                // var status = await _Context.SaveChangesAsync();
                // if (status > 0 )
                // {
                //     var data = _Mapper.Map<Data.Data.Entity.PhieuDangKiMuaHang, PhieuDangKiMuaHangModels>(entity);
                //     return new ResponseObject<PhieuDangKiMuaHangModels>(data, "Thêm thành công");
                // }
                // return new ResponseError(Code.NotFound, "Thêm thất bại");
                var entity = _Mapper.Map<Data.Data.Entity.PhieuDangKiMuaHang>(model);
                _Context.Add(entity);
                await _Context.SaveChangesAsync();
                var result = _Mapper.Map<PhieuDangKiMuaHangModels>(entity);
                return new ResponseObject<PhieuDangKiMuaHangModels>(result);
            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);

            }
        }

        public async Task<Response> createPhieuDangKiMuaHang(PhieuDangKiCreateModel[] model)
        {
            try
            {
                var entity = new List<Data.Data.Entity.PhieuDangKiMuaHang>();
                foreach (var item in model)
                {
                    entity.Add(new Data.Data.Entity.PhieuDangKiMuaHang(){ IdNhanVien = item.IdNhanVien, NgayTao = DateTime.Now });
                }
                
                await _Context.AddRangeAsync(entity);
                var status = await _Context.SaveChangesAsync();
                if (status > 0)
                {
                    var data = _Mapper.Map<Data.Data.Entity.PhieuDangKiMuaHang, PhieuDangKiMuaHangModels>(new Data.Data.Entity.PhieuDangKiMuaHang(){ IdNhanVien = model.First().IdNhanVien});
                    return new ResponseObject<PhieuDangKiMuaHangModels>(data, "Thêm thành công");
                }
                return new ResponseError(Code.NotFound, "Thêm thất bại");
            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);
            }
        }

        public async Task<Response> updatePhieuDangKiMuaHangg(Guid Id, PhieuDangKiUpdateModel model)
        {
            try
            {
                var entityModel = await _Context.phieuDangKiMuaHangs.Where(c => c.Id == Id).FirstOrDefaultAsync();
                if (entityModel == null)
                {
                    
                    return new ResponseError(Code.NotFound, "Không tìm thấy Id");
                }
                entityModel.IdNhanVien = model.IdNhanVien;
                var status = await _Context.SaveChangesAsync();
                if (status > 0)
                {
                    
                    var data = _Mapper.Map<Data.Data.Entity.PhieuDangKiMuaHang,PhieuDangKiMuaHangModels>(entityModel);
                    return new ResponseObject<PhieuDangKiMuaHangModels>(data, "Sửa thành công");
                }
                
                return new ResponseError(Code.NotFound, "Sửa thất bại");
            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);
            }
        }

        public async Task<Response> getPhieuDangKiMuaHangg(PhieuDangKiQueryModel filter)
        {
            try
            {
                var predicate = BuildQueryPhieuDangKi(filter);
                var result = _Context.phieuDangKiMuaHangs.Include(c=>c.NhanVien).GetPage(filter);
                var PhieuDangKiDTo =
                    JsonConvert.DeserializeObject<Pagination<PhieuDangKiMuaHangModels>>(JsonConvert.SerializeObject(result));
                return new ResponsePagination<PhieuDangKiMuaHangModels>(PhieuDangKiDTo);
            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);
            }
        }

        public async Task<Response> getIPhieuDangKiMuaHangg(Guid Id)
        {
            try
            {
                var entity = await _Context.phieuDangKiMuaHangs
                    .Where(c => c.Id == Id).FirstOrDefaultAsync();
                if (entity == null)
                {
                    return new ResponseError(Code.NotFound, "Không tìm thấy id ");
                }
                var data = _Mapper.Map<Data.Data.Entity.PhieuDangKiMuaHang, PhieuDangKiMuaHangModels>(entity);
                return new ResponseObject<PhieuDangKiMuaHangModels>(data);

            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);

            }
        }

        public async Task<Response> deletePhieuDangKiMuaHang(Guid Id)
        {
            try
            {
                var entity = await _Context.phieuDangKiMuaHangs.Where(c => c.Id == Id).FirstOrDefaultAsync();
                if (entity == null)
                {
                    return new ResponseError(Code.NotFound, "Không tìm thấy id để xóa");
                }

                _Context.Remove(entity);
                _Context.SaveChangesAsync();
                var ten = entity.IdNhanVien.ToString();
                return new ResponseDelete(Code.OK, "Xóa thành công", Id, ten);
            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);

            }
        }
        private Expression<Func<Data.Data.Entity.PhieuDangKiMuaHang, bool>> BuildQueryPhieuDangKi(PhieuDangKiQueryModel query)
        {
            var predicate = PredicateBuilder.New<Data.Data.Entity.PhieuDangKiMuaHang>(true);

            if (query.phieuDangKiPhanId.HasValue && query.phieuDangKiPhanId.Value != Guid.Empty)
            {
                predicate = predicate.And(c => c.Id == query.phieuDangKiPhanId);
            }


            return predicate;
        }

    }
}
