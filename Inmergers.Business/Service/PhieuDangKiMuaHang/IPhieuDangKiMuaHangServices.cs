﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Inmergers.Business.Service.MatHang;
using Inmergers.Common.Utils;

namespace Inmergers.Business.Service.PhieuDangKiMuaHang
{
    public interface IPhieuDangKiMuaHangServices
    {
        Task<Response> createPhieuDangKiMuaHang(PhieuDangKiCreateModel model);
        Task<Response> createPhieuDangKiMuaHang(PhieuDangKiCreateModel[] model);
        Task<Response> updatePhieuDangKiMuaHangg(Guid Id, PhieuDangKiUpdateModel model);
        Task<Response> getPhieuDangKiMuaHangg(PhieuDangKiQueryModel filter);
        Task<Response> getIPhieuDangKiMuaHangg(Guid Id);
        Task<Response> deletePhieuDangKiMuaHang(Guid Id);
        
    }
}
