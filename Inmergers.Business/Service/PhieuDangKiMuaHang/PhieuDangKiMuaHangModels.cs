﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Inmergers.Business.Service.NhanVien;
using Inmergers.Common.Utils;
using Inmergers.Data.Data.Entity;

namespace Inmergers.Business.Service.PhieuDangKiMuaHang
{
    public class PhieuDangKiMuaHangModels
    {
        public Guid Id { get; set; }
        public Guid IdNhanVien { get; set; }
        public List<ChiTietPhieuDangKiModel> ChiTietPhieuDangKi { get; set; }
    }
    public class PhieuDangKiCreateModel
    {
        public Guid IdNhanVien { get; set; }
        public List<ChiTietPhieuDangKiCreate> MatHangCreate { get; set; }

    }

     public class ChiTietPhieuDangKiModel
     {
         public Guid Id { get; set; }
         public int SoLuong { get; set; }
         public Guid IdMatHang { get; set; }
     }
     public class ChiTietPhieuDangKiCreate
     {
         public Guid IdMatHang { get; set; }
         public int soluong { get; set; }
        
     }

    public class PhieuDangKiUpdateModel
    {
        public Guid IdNhanVien { get; set; }
        public List<ChiTietPhieuDangKiCreate> ChiTietPhieuDangKi { get; set; }
    }
    public class PhieuDangKiQueryModel : PaginationRequest
    {
        public Guid? phieuDangKiPhanId { get; set; }
    }
}
