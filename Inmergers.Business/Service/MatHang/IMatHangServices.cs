﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Inmergers.Business.Service.BoPhan;
using Inmergers.Common.Utils;

namespace Inmergers.Business.Service.MatHang
{
    public interface IMatHangServices
    {
        Task<Response> createMatHang(MatHangCreateModels model);
        Task<Response> updateMatHang(Guid Id, MatHangUpdateModels model);
        Task<Response> getMatHang(MatHangQueryModel filter);
        Task<Response> getIdMatHang(Guid Id);
        Task<Response> deleteMatHang(Guid Id);
    }
}
