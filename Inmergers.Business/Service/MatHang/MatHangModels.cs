﻿using System;
using System.Collections.Generic;
using System.Text;
using Inmergers.Common.Utils;

namespace Inmergers.Business.Service.MatHang
{
    public class MatHangModels
    {
        public Guid Id { get; set; }
        public string tenMatHang { get; set; }
        public int Stt { get; set; }
        public int soLuong { get; set; }
    }
    public class MatHangCreateModels
    {
        public string tenMatHang { get; set; }
        public int Stt { get; set; }
        public int soLuong { get; set; }
    }
    public class MatHangUpdateModels
    {
        public string tenMatHang { get; set; }
        public int Stt { get; set; }
        public int soLuong { get; set; }
    }
    public class MatHangQueryModel : PaginationRequest
    {
        public Guid? MatHangId { get; set; }
    }
}
