﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Inmergers.Business.Service.BoPhan;
using Inmergers.Common;
using Inmergers.Common.Utils;
using Inmergers.Data.Data.DbContext;
using Inmergers.Data.Data.Entity;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Serilog;
using Code = System.Net.HttpStatusCode;

namespace Inmergers.Business.Service.MatHang
{
    public class MatHangServices : IMatHangServices
    {
        private readonly vattudbContext _Context;
        private readonly IMapper _Mapper;

        public MatHangServices(vattudbContext context, IMapper mapper)
        {
            _Context = context ?? throw new ArgumentNullException(nameof(context));
            _Mapper = mapper ?? throw new ArgumentNullException(nameof(mapper)); ;
        }

        public async Task<Response> createMatHang(MatHangCreateModels model)
        {
            try
            {
                var entityModel = new Data.Data.Entity.MatHang
                {
                 Stt = model.Stt,
                 soLuong = model.soLuong,
                 tenMatHang = model.tenMatHang,
                };
                _Context.Add(entityModel);
                var status = await _Context.SaveChangesAsync();
                if (status > 0)
                {
                    var data = _Mapper.Map<Data.Data.Entity.MatHang, MatHangModels>(entityModel);
                    return new ResponseObject<MatHangModels>(data, "Thêm thành công");
                }
                return new ResponseError(Code.NotFound, "Thêm thất bại");

            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);

            }
        }

        public async Task<Response> updateMatHang(Guid Id, MatHangUpdateModels model)
        {
            try
            {
                var entityModel = await _Context.matHangs.Where(c => c.Id == Id).FirstOrDefaultAsync();
                if (entityModel == null)
                {
                    return new ResponseError(Code.NotFound, "không tìm thấy Id mã hàng");
                }

                entityModel.Stt = model.Stt;
                entityModel.tenMatHang = model.tenMatHang;
                entityModel.soLuong = model.soLuong;
               
                var status = await _Context.SaveChangesAsync();
                if (status > 0)
                {
                    var data = _Mapper.Map<Data.Data.Entity.MatHang, MatHangModels>(entityModel);
                    return new ResponseObject<MatHangModels>(data, "Sửa thành công");
                }
                return new ResponseError(Code.NotFound, "Sửa thất bại");
            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);
            }
        }

        public async Task<Response> getMatHang(MatHangQueryModel filter)
        {
            try
            {
                var predicate = BuildQueryMatHang(filter);
                var result = _Context.matHangs.Where(predicate).GetPage(filter);
                var matHangDTo =
                    JsonConvert.DeserializeObject<Pagination<MatHangModels>>(JsonConvert.SerializeObject(result));
                return new ResponsePagination<MatHangModels>(matHangDTo);
            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);
            }
        }

        public async Task<Response> getIdMatHang(Guid Id)
        {
            try
            {
                var entity = await _Context.matHangs.Where(c => c.Id == Id).FirstOrDefaultAsync();
                if (entity == null)
                {
                    return new ResponseError(Code.NotFound, "Không tìm thấy Id Mã hàng");
                }

                var data = _Mapper.Map<Data.Data.Entity.MatHang, MatHangModels>(entity);
                return new ResponseObject<MatHangModels>(data);
            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);
            }
        }

        public async Task<Response> deleteMatHang(Guid Id)
        {
            try
            {
                var entity = await _Context.matHangs.Where(c => c.Id == Id).FirstOrDefaultAsync();
                if (entity == null)
                {
                    return new ResponseError(Code.NotFound, "Không tìm thấy Id mã hàng");
                }
                _Context.Remove(entity);
                _Context.SaveChanges();
                var tile = entity.tenMatHang;
                return new ResponseDelete(Code.OK, "Xóa thành công", Id, tile);

            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);
            }
        }
        private Expression<Func<Data.Data.Entity.MatHang, bool>> BuildQueryMatHang(MatHangQueryModel query)
        {
            var predicate = PredicateBuilder.New<Data.Data.Entity.MatHang>(true);

            if (query.MatHangId.HasValue && query.MatHangId.Value != Guid.Empty)
            {
                predicate = predicate.And(c => c.Id == query.MatHangId);
            }

            if (!string.IsNullOrEmpty(query.FullTextSearch))
                predicate.And(c => c.tenMatHang.Contains(query.FullTextSearch));

            return predicate;
        }

    }
}
