﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Inmergers.Common.Utils;
using Inmergers.Data.Data.Entity;

namespace Inmergers.Business.Service.NhanVien
{
    public class NhanVienModel
    {
        public Guid Id { get; set; }
        public string Hoten { get; set; }
        public string TenTk { get; set; }
        public string matkhau { get; set; }
        public string gmail { get; set; }
        public DateTime ngaytao { get; set; }
        public Guid IdBoPhan { get; set; }

    }

    public class CreateNhanvienandUpdate
    {
        public string Hoten { get; set; }
        public string TenTk { get; set; }
        public string matkhau { get; set; }
        public string gmail { get; set; }
        public DateTime ngaytao { get; set; }
        public Guid IdBoPhan { get; set; }
    }

    public class dangNhapNhanVien
    {
        public string TenTk { get; set; }
        public string matkhau { get; set; }
    }

    public class dangKyNhanVien
    {
        
        public string Hoten { get; set; }
        public string TenTk { get; set; }
        public string matkhau { get; set; }
        public string gmail { get; set; }
    }
    public class NguoiDungQueryModel : PaginationRequest
    {
        public Guid? IdNhanVien { get; set; }
    }
}
