﻿using Inmergers.Business.Service.BoPhan;
using Inmergers.Common.Utils;
using Inmergers.Data.Data.DbContext;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Code = System.Net.HttpStatusCode;

namespace Inmergers.Business.Service.NhanVien
{
    public class NhanVienServices : INhanVienServices
    {
        private readonly vattudbContext _context;
        private readonly Microsoft.Extensions.Configuration.IConfiguration _configuration;
        public NhanVienServices( vattudbContext context, Microsoft.Extensions.Configuration.IConfiguration config)
        {
            _context = context;

            _configuration = config;
        }

        public async Task<Response> Login(dangNhapNhanVien model)
        {
            var user = await _context.NhanViens.FirstOrDefaultAsync(c=>c.TenTk == model.TenTk);
            if (user == null)
            {
                return new ResponseError(Code.NotFound, "không tìm thấy tài khoản");
            }
            
            // if (!BCrypt.Net.BCrypt.Verify(model.matkhau, user.matkhau))
            // return new ResponseError(Code.BadRequest, "Mật khẩu không đúng");

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                new Claim("TenTk", user.TenTk),
                new Claim("MatKhau", user.matkhau),
                new Claim("gmail", user.gmail),
                new Claim("hoten", user.Hoten),
                new Claim("IdBoPhan", user.IdBoPhan.ToString()),
                new Claim("NgayTao", user.ngaytao.ToShortDateString()),
                new Claim("Id", user.Id.ToString())
            };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:key"]));
            var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(_configuration["Jwt:Issuer"], _configuration["Jwt:Audience"], claims, expires: DateTime.UtcNow.AddDays(1), signingCredentials: signIn);
            return new ResponseObject<string>(new JwtSecurityTokenHandler().WriteToken(token));
        }

        public Task<Response> Register(dangKyNhanVien model)
        {
            throw new NotImplementedException();
        }
    }
}
