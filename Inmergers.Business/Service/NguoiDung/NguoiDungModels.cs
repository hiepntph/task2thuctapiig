﻿using System;
using System.Collections.Generic;
using System.Text;
using Inmergers.Common.Utils;

namespace Inmergers.Business.Service.NguoiDung
{
    public class NguoiDungModels
    {
        public Guid Id { get; set; }
        public string TenTaiKhoan { get; set; }
        public string MatKhau { get; set; }
        public string HoTen { get; set; }
        public string Email { get; set; }
        public DateTime NgayTao { get; set; }
    }

    public class CreateNguoiDung
    {
        public string TenTaiKhoan { get; set; }
        public string MatKhau { get; set; }
        public string HoTen { get; set; }
        public string Email { get; set; }
        public DateTime NgayTao { get; set; }
    }
    public class UpdateNguoiDung
    {
        public string TenTaiKhoan { get; set; }
        public string MatKhau { get; set; }
        public string HoTen { get; set; }
        public string Email { get; set; }
        public DateTime NgayTao { get; set; }
    }

    public class NguoiDungQueryModel : PaginationRequest
    {
        public Guid? nguoidungId { get; set; }
    }
}
