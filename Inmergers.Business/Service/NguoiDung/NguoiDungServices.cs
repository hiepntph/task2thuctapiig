﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Inmergers.Common;
using Inmergers.Data.Data.DbContext;
using Serilog;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Inmergers.Common.Utils;
using Inmergers.Data.Data.Entity;
using LinqKit;
using Newtonsoft.Json;
using Code = System.Net.HttpStatusCode;
namespace Inmergers.Business.Service.NguoiDung
{
    public class NguoiDungServices : INguoiDungServices
    {
        private readonly vattudbContext _Context;
        private readonly IMapper _Mapper;

        public NguoiDungServices(vattudbContext context, IMapper mapper)
        {
            _Context = context ?? throw new ArgumentNullException(nameof(context));
            _Mapper = mapper ?? throw new ArgumentNullException(nameof(mapper)); ;
        }

        public async Task<Response> createNguoiDung(CreateNguoiDung model)
        {
            try
            {
                var entityModel = new Data.Data.Entity.NguoiDung()
                {
                    TenTaiKhoan = model.TenTaiKhoan,
                    HoTen = model.HoTen,
                    MatKhau = model.MatKhau,
                    Email = model.Email,
                    NgayTao = DateTime.Now,
                };
                _Context.Add(entityModel);
                var status = await _Context.SaveChangesAsync();
                if (status > 0)
                {
                    var data = _Mapper.Map<Data.Data.Entity.NguoiDung, NguoiDungModels>(entityModel);
                    return new ResponseObject<NguoiDungModels>(data, "Thêm thành công");
                }
                return new ResponseError(Code.NotFound, "Thêm thất bại");

            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);

            }
        }

        public async Task<Response> updateNguoiDung(Guid Id, UpdateNguoiDung model)
        {
            try
            {



                var entityModel = await _Context.nguoiDungs.Where(c => c.Id == Id).FirstOrDefaultAsync();
                if (entityModel == null)
                {
                    return new ResponseError(Code.NotFound, "không tìm thấy Id  người dùng");
                }

                entityModel.NgayTao = DateTime.Now;
                entityModel.HoTen = model.HoTen;
                entityModel.TenTaiKhoan = model.TenTaiKhoan;
                entityModel.Email = model.Email;
                entityModel.MatKhau = model.MatKhau;
                var status = await _Context.SaveChangesAsync();
                if (status > 0)
                {
                    var data = _Mapper.Map<Data.Data.Entity.NguoiDung, NguoiDungModels>(entityModel);
                    return new ResponseObject<NguoiDungModels>(data, "Sửa thành công");
                }

                return new ResponseError(Code.NotFound, "Sửa thất bại");
            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);
            }
        }

        public async Task<Response> getNguoiDung(NguoiDungQueryModel filter)
        {
            try
            {
                var predicate = BuildQueryNguoiDung(filter);
                var result = _Context.nguoiDungs.Where(predicate).GetPage(filter);
                var nguoiDungDto =
                    JsonConvert.DeserializeObject<Pagination<NguoiDungModels>>(JsonConvert.SerializeObject(result));

                return new ResponsePagination<NguoiDungModels>(nguoiDungDto);

            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);
            }
        }

        public async Task<Response> getIdNguoiDungg(Guid Id)
        {
            try
            {
                var entity = await _Context.nguoiDungs.Where(c => c.Id == Id).FirstOrDefaultAsync();
                if (entity == null)
                {
                    return new ResponseError(Code.NotFound, "Không tìm thấy Id bộ phận");
                }

                var data = _Mapper.Map<Data.Data.Entity.NguoiDung, NguoiDungModels>(entity);
                return new ResponseObject<NguoiDungModels>(data);
            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);
            }
        }

        public async Task<Response> deleteNguoiDung(Guid Id)
        {
            try
            {
                var entity = await _Context.nguoiDungs.Where(c => c.Id == Id).FirstOrDefaultAsync();
                if (entity == null)
                {
                    return new ResponseError(Code.NotFound, "Không tìm thấy Id người dùng");
                }

                _Context.Remove(entity);
                _Context.SaveChanges();
                var tile = entity.HoTen;
                return new ResponseDelete(Code.OK, "Xóa thành công", Id, tile);
            }
            catch (Exception e)
            {
                Log.Error(e, string.Empty);
                return new ResponseError(Code.InternalServerError, e.Message);
            }
        }
        private Expression<Func<Data.Data.Entity.NguoiDung, bool>> BuildQueryNguoiDung(NguoiDungQueryModel query)
        {
            var predicate = PredicateBuilder.New<Data.Data.Entity.NguoiDung>(true);

            if (query.nguoidungId.HasValue && query.nguoidungId.Value != Guid.Empty)
            {
                predicate = predicate.And(c => c.Id == query.nguoidungId);
            }

            if (!string.IsNullOrEmpty(query.FullTextSearch))
                predicate.And(c => c.TenTaiKhoan.Contains(query.FullTextSearch)
                                   || c.HoTen.Contains(query.FullTextSearch) || c.Email.Contains(query.FullTextSearch));

            return predicate;
        }

    }
}
