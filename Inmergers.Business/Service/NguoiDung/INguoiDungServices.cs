﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Inmergers.Common.Utils;

namespace Inmergers.Business.Service.NguoiDung
{
    public interface INguoiDungServices
    {
        Task<Response> createNguoiDung( CreateNguoiDung model);
        Task<Response> updateNguoiDung(Guid Id, UpdateNguoiDung model);
        Task<Response> getNguoiDung(NguoiDungQueryModel filter);
        Task<Response> getIdNguoiDungg(Guid Id);
        Task<Response> deleteNguoiDung(Guid Id);
    }
}
