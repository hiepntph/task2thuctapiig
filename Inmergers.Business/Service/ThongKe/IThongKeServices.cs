﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Inmergers.Common.Utils;

namespace Inmergers.Business.Service.ThongKe
{
    public interface IThongKeServices
    {
        Task<Response> ThongKeSoLuongNhanVienTheoThang();
        Task<Response> ThongKeSoLuongNhanVienTrongBoPhan();
        Task<Response> ThongKeSoLuongSanPhamDangkyTheoThang();
    }
}
