﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Inmergers.Common.Utils;
using Inmergers.Data.Data.DbContext;
using Inmergers.Data.Data.Entity;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace Inmergers.Business.Service.ThongKe
{
    public class ThongkeServices : IThongKeServices
    {

        private readonly vattudbContext _Context;

        public ThongkeServices(vattudbContext context)
        {
            _Context = context;
        }

        public async Task<Response> ThongKeSoLuongNhanVienTheoThang()
        {
            try
            {
                // var result = await _Context.NhanViens
                //     .GroupBy(x => x.ngaytao.ToString("yyyy-MM"))
                //     .Select(c => new SoLuongThangNam
                //     {
                //         nam = int.Parse(c.Key.Split("-")[0]),
                //         thang = int.Parse(c.Key.Split("-")[1]),
                //         soluong = c.Count()
                //     })
                //     .ToListAsync();
                var slNhanVienTheoThang = new Dictionary<string, int>();

                foreach (var nhanvien in _Context.NhanViens)
                {
                    var ngayvao = nhanvien.ngaytao.ToString("yyyy-MM");
                    if (!slNhanVienTheoThang.ContainsKey(ngayvao))
                        slNhanVienTheoThang[ngayvao] = 1;
                    else
                        slNhanVienTheoThang[ngayvao] += 1;
                }

                var result = slNhanVienTheoThang
                    .Select(c => new SoLuongThangNam
                    {
                        nam = int.Parse(c.Key.Split("-")[0]),
                        thang = int.Parse(c.Key.Split("-")[1]),
                        soluong = c.Value
                    })
                    .ToList();
                return new ResponseList<SoLuongThangNam>(result);

               
            }
            catch (Exception e)
            {
                Log.Error("báo lỗi");
                return new ResponseError(HttpStatusCode.InternalServerError, e.Message);
            }
          
        }


        public async Task<Response> ThongKeSoLuongNhanVienTrongBoPhan()
        {
            try
            {
                var result = await _Context.boPhans.Select(bophan => new SlNhanVienPhongBanModesl
                {
                    tenBoPhan = bophan.tenBoPhan,
                    soluong = bophan.nhanvien.Count
                }).ToListAsync();
                return new ResponseList<SlNhanVienPhongBanModesl>(result);
            }
            catch (Exception e)
            {
                Log.Error("báo lỗi");
                return new ResponseError(HttpStatusCode.InternalServerError, e.Message);
            }
           
        }

        public async Task<Response> ThongKeSoLuongSanPhamDangkyTheoThang()
        {
            try
            {
                
                    var sanPhamTheoThang = new Dictionary<string, int>();
                    foreach (var purchaseOrder in _Context.phieuDangKiMuaHangs)
                {
                    var key = purchaseOrder.NgayTao.ToString("yyyy-MM");
                    if (!sanPhamTheoThang.ContainsKey(key))
                        sanPhamTheoThang[key] = 1;
                    else
                        sanPhamTheoThang[key] += 1;
                }
                var result = sanPhamTheoThang
                    .Select(c => new SoLuongThangNam
                    {
                        nam = int.Parse(c.Key.Split("-")[0]),
                        thang = int.Parse(c.Key.Split("-")[1]),
                        soluong = c.Value
                    })
                    .ToList();
                return new ResponseList<SoLuongThangNam>(result);
            }
            catch (Exception e)
            {
                Log.Error("báo lỗi");
                return new ResponseError(HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}
