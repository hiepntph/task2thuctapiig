﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Inmergers.Business.Hubs.Models
{
    public class DangNhapModels
    {
        [Required]
        public string tenDangNhap { get; set; }
        [Required]
        public string MatKhau { get; set; }
    }
}
